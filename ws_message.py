import utils.my_debug_utils as dbg


class WebSocketMessageHeader:
    def __init__(self, msg):
        # starting index of a payload
        self.ind_s = 2
        self.msg_copy = ''
        self.isFinished = 0
        self.isMasked = 0
        self.flags = 0
        self.opcode = 0
        self.msg_len = 0
        self.maskingKey = 0

        self.fetch_header(msg)

    def fetch_header(self, msg):
        self.msg_copy = msg

        for i in self.msg_copy:
            i = int(i)

        self.isFinished = (self.msg_copy[0] & 0x80) >> 7
        self.flags = (self.msg_copy[0] & 0x70) >> 4
        self.opcode = (self.msg_copy[0] & 0x0f)
        self.isMasked = (self.msg_copy[1] & 0x80) >> 7
        self.msg_len = (self.msg_copy[1] & 0x7f)

        # check if received packet length field says it's extended length format
        if self.msg_len < 126:
            pass
        elif self.msg_len == 126:
            self.msg_len = (self.msg_copy[2] << 8) + self.msg_copy[3]
            self.ind_s += 2
        # 8-bytes len format
        elif self.msg_len == 127:
            # todo
            # self.msg_len =
            self.ind_s += 8
        else:
            dbg.print_error('Invalid header len.')

        # check if message is masked. It should be as it is stated in RFC6455 that client
        # side should always send 4 bytes of masking key and masked payload
        if self.isMasked == 1:
            self.maskingKey = self.msg_copy[self.ind_s:self.ind_s + 4]
            self.ind_s += 4


class WebSocketMessage:
    def __init__(self, data):
        self.header = WebSocketMessageHeader(data)
        # first it is masked
        self.payload = list(data[self.header.ind_s:])
        self.unmask_payload()

    def unmask_payload(self):
        for i in range(0, len(self.payload)):
            self.payload[i] = self.payload[i] ^ self.header.maskingKey[i % 4]
