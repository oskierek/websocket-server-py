from termcolor import colored
import datetime

current_time = datetime.datetime.now


def print_hex_list(data):
    for i in data:
        print(hex(i), end=', ')
    print()


def print_error(error):
    print(colored(error, 'red'))


def print_warning(warn):
    print(colored(warn, 'yellow'))
