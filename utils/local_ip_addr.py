import socket


class LocalIPAddress:
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.ip = '127.0.0.1'

    def __str__(self):
        try:
            # doesn't even have to be reachable
            self.s.connect(('10.255.255.255', 1))
            self.ip = self.s.getsockname()[0]
        except:
            self.ip = '127.0.0.1'
        finally:
            self.s.close()
        return self.ip
