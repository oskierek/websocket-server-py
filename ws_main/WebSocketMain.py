''' Supports ipv4 only '''
import socket as sock
from ws_handshake.ws_handshake import WebSocketHandshake
from termcolor import colored
from ws_message import WebSocketMessage as wsm
import utils.my_debug_utils as dbg


class WebSocket:
    def __init__(self, host, port):
        self.connectionEstablished = False
        self.port = port
        self.host_address = 0
        self.conn = -1

        self.server_socket = sock.socket(sock.AF_INET, sock.SOCK_STREAM)
        self.server_socket.bind((host, port))
        self.server_socket.listen(1)

        self.conn, self.client_addr = self.server_socket.accept()

        print(str(dbg.current_time()) + ': Client ' + str(self.client_addr[0]) + ' connected.')
        print('Waiting for a handshake...')
        hs_buf = self.conn.recv(1024)

        hs = WebSocketHandshake(hs_buf)
        if hs.hsAccept != '':
            self.conn.send(str(hs.hsResponse).encode('ascii'))
            self.connectionEstablished = True
            print(str(dbg.current_time()) + ': WebSocket connection established.')

            # start doing some smart things
            # starting sender thread to make communication duplex
            # self.sender_thread = WebSocketSenderThread(self)
            # receiving data here in the loop as long as the client is connected
            self.receiveLoop()
        else:
            alert = str(dbg.current_time()) + 'Received invalid handshake from ' + str(self.client_addr[0])
            print(colored(alert, 'red'))

    def receiveLoop(self):
        while self.connectionEstablished:
            try:
                rcv_msg = self.conn.recv(1024)
                # handling received message
                self.ws_msg = wsm(rcv_msg)
                self.msg_handler(self.ws_msg)
            except ConnectionResetError:
                alert = str(dbg.current_time()) + ': Connection was dropped by remote host.'
                print(colored(alert, 'red'))
                exit(-1)

    def ws_receive(self):
        pass

    def msg_handler(self, msg):
        opcode = self.ws_msg.header.opcode
        # continuation frame
        if opcode == 0x00:
            pass
        # text frame - simply print it to output
        elif opcode == 0x01:
            for i in self.ws_msg.payload:
                print(chr(i), end='')
            print()
        # binary frame
        elif opcode == 0x02:
            pass
        # connection close frame
        elif opcode == 0x08:
            print(str(dbg.current_time()) + ": Client requested closing WebSocket")
            self.ws_close_conn()
        # ping frame
        elif opcode == 0x09:
            pass
        # pong frame
        elif opcode == 0x0A:
            pass
        # invalid opcode
        else:
            pass

    def ws_send(self):
        pass

    def ws_close_conn(self):
        msg = [0x88, 0x02, 0x03, 0xe8]
        # close the connection and terminate the thread
        self.conn.send(bytes(msg))
        self.connectionEstablished = False
        self.conn.close()
        print('Connection closed')

    def __str__(self):
        if self.connectionEstablished:
            return 'WebSocket at port ' + str(self.port)
        else:
            return 'No client connected.'
