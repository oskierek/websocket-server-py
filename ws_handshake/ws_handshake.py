import hashlib
import base64

GUID = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11'


class WebSocketHandshake:
    def __init__(self, hs):
        self.hsIndS = -1
        self.hsIndT = -1
        self.hsKey = ''
        self.hsAccept = ''
        self.hsResponse = ''
        try:
            hs = hs.decode('ascii')
            hs_field_name = 'Sec-WebSocket-Key: '
            self.hsIndS = hs.find(hs_field_name) + len(hs_field_name)
            self.hsIndT = hs.find('\r\n', self.hsIndS)
            self.hsKey = hs[self.hsIndS:self.hsIndT]

        except UnicodeDecodeError:
            pass
        if self.hsKey != '':
            self.hsAccept = self.prepareHandshakeResponse()
            self.hsResponse = 'HTTP/1.1 101 Switching Protocols\r\n' \
                            'Upgrade: websocket\r\n' \
                            'Connection: Upgrade\r\n' \
                            'Sec-WebSocket-Accept: ' + self.hsAccept + '\r\n\r\n'

    def prepareHandshakeResponse(self):
        accept_key_response = self.hsKey + GUID
        encrypt_accept = hashlib.sha1()
        encrypt_accept.update(accept_key_response.encode('ascii'))
        tmp = encrypt_accept.digest()

        tmp = base64.b64encode(tmp)
        accept_key_response = tmp.decode()

        return accept_key_response
